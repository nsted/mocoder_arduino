void setupEncoder() {
  Wire.begin();  
}

// calculate the gap, which is the distance between the destination and current position.

long getGap(long dest, long pos) {
  long gap = dest - pos;
  return gap;
}



// Since the encoder only reports an angle,
// use rolloverAdjust() to track when servo
// rotates over 360 degrees in either direction

int rolloverAdjust(int angle) {
  static int lastAngle = angle;

  int rollover = 0;
  int angleDiff = angle - lastAngle;

  if (angleDiff > (STEPS - 1000) ) {
    rollover--;
  }
  else if (angleDiff < ((STEPS - 1000) * -1)) {
    rollover++;
  }

  lastAngle = angle;
  return rollover * STEPS;
}

