const int HOLD_PW = 1522;         // The pulsewidth for stopping, adjust for your servo's hold point
                                  // to find the hold point, visit <https://hackaday.io/project/17079-mocoder-magnetic-encoder>
                                  // or simply run the following program available at
                                  // https://bitbucket.org/nsted/continuous_servo_calibration
const int DIR = -1;               // 1 for normal tracking, -1 for reverse (change if gap grows instead of shrinking)
const int MAX_VEL_PW_DELTA = 100; // The pulsewidth for max speed in other direction



void setupMotions(){
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object  
}


int getPwDelta(){
  return MAX_VEL_PW_DELTA;
}


// Rotate the servo to the given destination given the current position.

void rotateTo(long gap) {
  // update the PID
  Input = gap;
  Setpoint = 0.0;
  pid.Compute();
  rotate(Output);
}



// Rotate the motor at the given velocity (+/- MAX_VEL_PW_DELTA).
// Function translates velocity value to pulsewidth required
// to set speed of a continuous servo.

int lastPulseWidth = 0;

void rotate(long vel) {
  int pulseWidth;
  if (DIR > 0) {
    pulseWidth = HOLD_PW + vel;
  }
  else {
    pulseWidth = HOLD_PW - vel;
  }

  // constrain pulseWidth to Max and Min
  if (pulseWidth > HOLD_PW + MAX_VEL_PW_DELTA) {
    pulseWidth = HOLD_PW + MAX_VEL_PW_DELTA;
  }
  if (pulseWidth < HOLD_PW - MAX_VEL_PW_DELTA) {
    pulseWidth = HOLD_PW - MAX_VEL_PW_DELTA;
  }

  // if changed update the servo {for TGY-S9010 servo) 
  if (pulseWidth != lastPulseWidth) {
    myservo.writeMicroseconds(pulseWidth);
  }
  lastPulseWidth = pulseWidth;
}



void debugMotions(long pos, long gap){
  Serial.print("timer: " + String(millis() - posTimeStamp));
  Serial.print(", pos: " + String(pos));
  Serial.print(", dest: " + String(dest));
  Serial.println(", gap: " + String(gap));
}

