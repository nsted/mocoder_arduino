/*****************************************************************************************

   This program is used to create a servo using a continuous RC servo motor and
   an AS5600-based magnetic encoder. It reads the current position from the encoder
   and compares it to a stored destination. The motor is then rotated until the
   destination is reached. An array of destinations is stored, and the servo moves
   to each in sequence with a short delay between them.

   An activation switch is toggled to activate or deactivate the motor. When deactivated
   the motor can manually moved to a new position. The new position can then be sampled
   by pressing the sample button. This will replace the former position with the new one.

   In this version, settings are not saved in EEPROM, so any rotations,
   and sampled positions will be lost on restart.

   https://hackaday.io/project/17079-mocoder-magnetic-encoder

   Created by Nick Stedman 2016, nick@robotstack.com.

 *****************************************************************************************/


#include <Wire.h>
#include "AMS_5600.h"
#include <Servo.h>
#include "PID_v1.h"

// define the circuit that you are using, either BASIC or SAMPLER
// more info on circuits available here:
// https://hackaday.io/project/17079-mocoder-magnetic-encoder
#define SAMPLER

Servo myservo;                        // create servo object to control a servo
AMS_5600 encoder;                     // create encoder object to read from the encoder

const long STEPS = 4096;               // 12 bit encoder == 4096 steps per rotation
const int POSITION_HOLD_TIME = 2000;  // Hold a position for this many miliseconds
const int KF_SAMPLE_INTERVAL = 1000;  // Only sample every so often, to prevent repeat samples
const int GAP_CLOSED = 200;           // Consider the gap closed if within this many steps
const int INACTIVE = HIGH;            // The state of the activity switch to be considered inactive

// customize these two:
const int KF_BUFFER_SIZE = 3;          // How many samples
long keyFrames[KF_BUFFER_SIZE] = {0, STEPS, STEPS * 10}; // the sequence of destinations

long dest = 0;
long rotations = 0;
long posTimeStamp = 0;
int kfIndex = 0;
int kfIncrement = 1;
long kfSampleTimeStamp = 0;


void setup() {
  setupButtons();
  setupEncoder();
  setupMotions();
  double maxVel = getPwDelta();
  setupPID(maxVel);
  Serial.begin(9600);
  delay(10);
  rotate(0);
}

void loop() {
  // get the current position
  long pos = encoder.getRawAngle();
  rotations += rolloverAdjust(pos);
  pos += rotations;

  // retrieve user specified destination if available
  dest = getIntFromSerial(dest);

  // update the gap
  long gap = getGap(dest, pos);

  // keep resetting the timer, until destination reached
  if (gap > GAP_CLOSED || gap < -GAP_CLOSED) {
    posTimeStamp = millis();
  }

  // check if the activation switch is in active or inactive state.
  int activationSwitchState = activationSwitchRead();
  if (activationSwitchState == INACTIVE) {
    // if inactive then sample position if button is pressed.
    int kfSampleButtonState = kfSampleButtonRead();
    if (kfSampleButtonState == LOW) {
      // only sample every second at most
      if (millis() - kfSampleTimeStamp > KF_SAMPLE_INTERVAL) {
        keyFrames[kfIndex] = pos;
        dest = keyFrames[kfIndex];
        Serial.println("new sample on " + String(kfIndex) + " == " + String(keyFrames[kfIndex]));
        // prep for next sample
        kfIndex++;
        if (kfIndex > KF_BUFFER_SIZE-1) {
          kfIndex = 0;
        }
        kfSampleTimeStamp = millis();
      }
    }
  }
  else {
    // retrieve next destination from the keyFrame array.
    if (millis() - posTimeStamp > POSITION_HOLD_TIME) {
      dest = keyFrames[kfIndex];
      gap = getGap(dest, pos);
      if(kfIndex >= KF_BUFFER_SIZE-1) {
        kfIncrement = -1;
      }
      if(kfIndex <= 0) {
        kfIncrement = 1;
      }
      kfIndex += kfIncrement;
      posTimeStamp = millis();
    }

    // now rotate to the destination
    rotateTo(gap);

//    debugMotions(pos, gap);
//    debugPID();
    plotPID();
  }
  delay(1);
}



// Retrieve a signed integer from the Serial port.
// Signed value should be a sent as ascii sequence and terminated with CR & NL.

String sInString = "";

long getIntFromSerial(long lastVal) {
  long newVal = lastVal;

  while (Serial.available() > 0) {
    int inChar = Serial.read();

    if (isDigit(inChar) || inChar == '-') {
      sInString += (char)inChar;
    }
    else if (inChar == '\n') {
      newVal = sInString.toInt();
      sInString = "";
    }
    else if (inChar == '\r') {
    }
    else {
      sInString = "";
    }
  }
  return newVal;
}
