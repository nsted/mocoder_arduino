double Setpoint;
double Input; 
double Output;       
double Kp = 0.25;                      // Proportional tuning
double Ki = 0.0;                       // Integral tuning
double Kd = 0.000;                     // Derivative tuning

PID pid(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);



void setupPID(double maxVel){
  Setpoint = 0.0;
  Input = 0.0;
  pid.SetOutputLimits(maxVel * -1, maxVel);
  pid.SetSampleTime(15);
  pid.SetMode(AUTOMATIC);
}



void debugPID(){
  Serial.print("Input: " + String(Input));
  Serial.print(", Setpoint: " + String(Setpoint));
  Serial.println(", Output: " + String(Output));
}


void plotPID(){
  double cappedInput;
  if (Input > 0) {
    cappedInput = min(100,Input);
  }
  else {
    cappedInput = max(100,Input);
  }

  Serial.print(100);
  Serial.print(" ");
  Serial.print(-100);
  Serial.print(" ");  
  Serial.print((int)cappedInput);
  Serial.print(" ");
  Serial.print((int)Setpoint);
  Serial.print(" ");
  Serial.println((int)Output);  
}

