const int ACTIVATION_SWITCH_PIN = 4;
const int KF_SAMPLE_BUTTON_PIN = 3;
const int DEBOUNCE_DELAY = 50;        // the debounce time; increase if the output flickers


void setupButtons() {
  #ifdef SAMPLER
  // used if sampling circuit is used
  pinMode(ACTIVATION_SWITCH_PIN, INPUT);
  #else
  // used if basic circuit is used <https://hackaday.io/project/17079-mocoder-magnetic-encoder>
  pinMode(ACTIVATION_SWITCH_PIN, INPUT_PULLUP);
  #endif
  
  pinMode(KF_SAMPLE_BUTTON_PIN, INPUT_PULLUP);  
}


// read the state of the Keyframe sample button
// using a debounce routine

unsigned long kfSampleButtonTimeStamp = 0;       // the last time the output pin was toggled
int kfSampleButtonState = LOW;               // the current reading from the input pin
int kfSampleButtonLastState = LOW;           // the last reading from the input pin

int kfSampleButtonRead() {
  int reading = digitalRead(KF_SAMPLE_BUTTON_PIN);

  // wait for the noise to settle
  if (reading != kfSampleButtonLastState) {
    kfSampleButtonTimeStamp = millis();
  }

  if ((millis() - kfSampleButtonTimeStamp) > DEBOUNCE_DELAY) {
    if (reading != kfSampleButtonState) {
      kfSampleButtonState = reading;
    }
  }
  
  kfSampleButtonLastState = reading;
  return kfSampleButtonState;
}




// read the state of the activation switch
// using a debounce routine

unsigned long activationSwitchTimeStamp = 0;       // the last time the output pin was toggled
int activationSwitchState = LOW;               // the current reading from the input pin
int activationSwitchLastState = LOW;           // the last reading from the input pin

int activationSwitchRead() {
  int reading = digitalRead(ACTIVATION_SWITCH_PIN);

  // wait for the noise to settle
  if (reading != activationSwitchLastState) {
    activationSwitchTimeStamp = millis();
  }

  if ((millis() - activationSwitchTimeStamp) > 50) {
    if (reading != activationSwitchState) {
      activationSwitchState = reading;
    }
  }
  
  activationSwitchLastState = reading;
  return activationSwitchState;
}
