This program is used to create a servo using a continuous RC servo motor and
an AS5600-based magnetic encoder. It reads the current position from the encoder
and compares it to a stored destination. The motor is then rotated until the
destination is reached. An array of destinations is stored, and the servo moves
to each in sequence with a short delay between them.

An activation switch is toggled to activate or deactivate the motor. When deactivated
the motor can manually moved to a new position. The new position can then be sampled
by pressing the sample button. This will replace the former position with the new one.

In this version, settings are not saved in EEPROM, so any rotations,
and sampled positions will be lost on restart.

Follow the tutorial here for setup details:
https://hackaday.io/project/17079-mocoder-magnetic-encoder

Created by Nick Stedman 2016, nick@robotstack.com.